var gulp = require('gulp'),
    sass = require('gulp-sass'),
    scsslint = require('gulp-scss-lint'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
    return gulp.src('./src/scss/**/init.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('scss-lint', function() {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(scsslint());
});

gulp.task('watch', function () {
    gulp.watch('./src/scss/**/*.scss', ['scss-lint', 'sass']);
    gulp.watch('./src/images/**/*.{gif,jpg,png,svg', ['images']);
    gulp.watch('./src/fonts/**/*.{eot,svg,ttf,woff,woff2}', ['fonts']);
});

gulp.task('fonts', function () {
    return gulp.src('./src/fonts/**/*.{eot,svg,ttf,woff,woff2}')
        .pipe(gulp.dest('./dist/fonts/'));
});

gulp.task('images', function () {
    return gulp.src('./src/images/**/*.{gif,jpg,png,svg}')
        .pipe(gulp.dest('./dist/images'));
});

gulp.task('default', ['sass-lint', 'sass']);
