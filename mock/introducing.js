var introducingElements = [{
    title: "Web & UI Design",
}, {
    title: "Corporate Design",
}, {
    title: "HTML 5 Development",
}]

module.exports = {
    introducingElements: introducingElements,
}