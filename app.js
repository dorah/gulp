var express = require('express'),
    hbs = require('express-handlebars'),
    path = require('path'),
    introducing = require('./mock/introducing');

var app = express(),
    handlebars = hbs.create({
        extname: 'hbs',
        defaultLayout: 'layout',
        layoutsDir: __dirname + '/views/layouts/'
    });

app.engine('hbs', handlebars.engine);
app.use(express.static(__dirname + '/dist/'));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.get('/', function (req, res) {
    res.render('index', {
        introducingElements: introducing.introducingElements
    });
});

app.listen(4000);